<?php
	
	require("khan/khan-util.php");
	
	$name = $_GET['khan'];
	$file = "khan/khan-$name.php";
	
	$prettyName = prettifyName($name);
	
?>
<!DOCTYPE html>
<html>
	
	<head>
		
		<?php include("lib/jd3res-headres.php"); ?>
		<title>James Del Bonis III :: APCSP Khan Academy $prettyName</title>
		
	</head>
	
	<body>
		
		<div id="content">
			
			<?php 
				include("lib/jd3res-heading.php");
			?>
			
			<div class="panel">
				
				<h2>Khan Item: <?php echo $prettyName; ?></h2>
				<p class="code"><?php echo $file; ?></p>
				
				<hr/>
				<?php makeKhanPanel($file); ?>
				
			</div>
			
		</div>
		
	</body>
	
</html>
