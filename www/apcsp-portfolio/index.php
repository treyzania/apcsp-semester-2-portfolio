<?php
	
	require("khan/khan-util.php");
	
	$khans = array('dancing', 'racing', 'scalable-dancing', 'scalable-racing', 'house', 'face', 'bunny', 'bear', 'frog', 'animal');
	
?>
<!DOCTYPE html>
<html>
	
	<head>
		
		<?php include("lib/jd3res-headres.php"); ?>
		<title>James Del Bonis III :: APCSP Portfolio Project</title>
		
	</head>
	
	<body>
		
		<div id="content">
			
			<?php include("lib/jd3res-heading.php"); ?>
			
			<div id="khannav" class="panel">
				
				<h3>This is where all the Khan stuff goes</h3>
				
				<ul>
				<?php
					
					foreach($khans as $khanItem) {
						
						$pretty = prettifyName($khanItem);
						echo "<li><a href=\"/apcsp-portfolio/khanjs.php?khan=$khanItem\">$pretty</a></li>\n";
						
					}
					
				?>
				</ul>
				
			</div>
			
		</div>
		
	</body>
	
</html>

